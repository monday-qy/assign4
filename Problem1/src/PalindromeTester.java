//********************************************************************
//File:         Palindrome.java       
//Author:       YANSHUN QIU
//Date:         Oct. 19th
//Course:       CPS100
//
//Problem Statement:
//create a modified version of the PalindromeTester program so that the spaces,
//punctuation, and changes in uppercase and lowercase are not considered when 
//determining whether a string is palindrome. Hint: These issues can be handled in 
//serval ways. Think carefully about your design.
//
//Inputs:   
//Outputs:   result of whether the input is palindrome
//********************************************************************



import java.util.Scanner;

public class PalindromeTester
{

  private static boolean isLetterOrDigit(char ch)
  {
    if(ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <= 'z' ||
       ch >= '0' && ch <= '9') {
      return true;
    }
    return false;
  }
  public static void main (String[] args)
  {
    String str, another = "y";
    int left, right;
    Scanner scan = new Scanner (System.in);
    Character x, y;

    while (another.equalsIgnoreCase("y")) 
    {
      System.out.println ("Enter a potential palindrome:");
      str = scan.nextLine();
      
      left = 0;
      right = str.length() - 1;
      
      
      while(left < right) {
        x = str.charAt(left);
        y = str.charAt(right);
        
        assert Character.isLetterOrDigit(x) == isLetterOrDigit(x);
        assert Character.isLetterOrDigit(y) == isLetterOrDigit(y);
        
        if(Character.toLowerCase(x) ==
           Character.toLowerCase(y)) {
          left++;
          right--;
        }
        else if(x == y) {
          left++;
          right--;
        }
 
        else if(!isLetterOrDigit(x)) {
          left++;
          x = str.charAt(left);
          if(!isLetterOrDigit(y)) {
            right--;
            y = str.charAt(right);
          }
        }
        else if(!isLetterOrDigit(y)) {
           right--;
           y = str.charAt(right);
           if(!isLetterOrDigit(x)) {
             
             assert false;
             left++;
             x = str.charAt(left);
           }
        }
        else {
          break;
        }
	    }
      while (str.charAt(left) == str.charAt(right) && left < right)
      System.out.println();
  
      if (left < right) {
        System.out.println ("That string is NOT a palindrome.");
      }
      else {
        System.out.println ("That string IS a palindrome.");
      }
  
      System.out.println();
      System.out.print ("Test another palindrome (y/n)? ");
  
      another = scan.nextLine();
    }
  }
}