//********************************************************************
//File:         Palindrome.java       
//Author:       YANSHUN QIU
//Date:         Oct. 19th
//Course:       CPS100
//
//Problem Statement:
//Design and implement a program that counts the number of punctuation
//marks in a text input file. Produce a table that shows how many times
//occurred.
//
//Inputs:    None
//Outputs:   Table listing the number of times each punctuation mark appeared
//           in a text input file
//********************************************************************

import java.io.*;


public class BookReader
{
  
//Creates an object of the class BookScanner and uses a method therein. 
  public static void main(String[] args) throws FileNotFoundException
  {
    BookScanner frankenstein = new BookScanner();

    System.out.println(frankenstein.listPunctuation("frankenstein.txt"));
  }
}