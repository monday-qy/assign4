//********************************************************************
//File:         Palindrome.java       
//Author:       YANSHUN QIU
//Date:         Oct. 19th
//Course:       CPS100
//
//Problem Statement:
//Design and implement a program that counts the number of punctuation
//marks in a text input file. Produce a table that shows how many times
//occurred.
//
//Inputs:    None
//Outputs:   Table listing the number of times each punctuation mark appeared
//           in a text input file.
//********************************************************************



import java.util.*;
import java.io.*;


public class BookScanner
{

  public BookScanner()
  {
  }

  public String listPunctuation(String strFileName) throws FileNotFoundException
  {

    String strTable = "";
    String strTemp = "";
    char charReader;
    int iLeft = 0, iRight = 0;
    final char CPERIOD = '.', CCOMMA = ',', CEXCLAMATION = '!';
    final char CQUESTION = '?', CSEMICOLON = ';', CCOLON = ':';
    final char CQUOTATION = '"', CAPOSTROPHE = '\'';
    int iPeriod = 0, iComma = 0, iExclamation = 0, iQuestion = 0;
    int iSemicolon = 0, iColon = 0, iQuotation = 0, iApostrophe = 0;

    Scanner fileScanner = new Scanner(new File(strFileName));

    while (fileScanner.hasNext())
    {
      strTemp = fileScanner.next();
      iLeft = 0;
      iRight = strTemp.length();

      
      while (iLeft < iRight)
      {
        charReader = strTemp.charAt(iLeft);
        if (charReader == CPERIOD)
        {
          iPeriod++;
          iLeft++;
        }
        else if (charReader == CCOMMA)
        {
          iComma++;
          iLeft++;
        }
        else if (charReader == CEXCLAMATION)
        {
          iExclamation++;
          iLeft++;
        }
        else if (charReader == CQUESTION)
        {
          iQuestion++;
          iLeft++;
        }
        else if (charReader == CSEMICOLON)
        {
          iSemicolon++;
          iLeft++;
        }
        else if (charReader == CCOLON)
        {
          iColon++;
          iLeft++;
        }
        else if (charReader == CQUOTATION)
        {
          iQuotation++;
          iLeft++;
        }
        else if (charReader == CAPOSTROPHE)
        {
          iApostrophe++;
          iLeft++;
        }
        else
        {
          iLeft++;
        }
      }
    }

    strTable += "Punctuation marks present within the scanned text: ";
    strTable += "\n'.': " + iPeriod;
    strTable += "\n',': " + iComma;
    strTable += "\n'!': " + iExclamation;
    strTable += "\n'?': " + iQuestion;
    strTable += "\n';': " + iSemicolon;
    strTable += "\n':': " + iColon;
    strTable += "\n'\"': " + iQuotation;
    strTable += "\n'\'': " + iApostrophe;
    
    fileScanner.close();
    return strTable;
  }
}